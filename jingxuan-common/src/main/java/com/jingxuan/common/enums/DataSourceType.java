package com.jingxuan.common.enums;

/**
 * 数据源
 * 
 * @author 精选
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
