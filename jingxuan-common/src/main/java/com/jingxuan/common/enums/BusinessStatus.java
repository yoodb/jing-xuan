package com.jingxuan.common.enums;

/**
 * 操作状态
 * 
 * @author 精选
 *
 */
public enum BusinessStatus {
	/**
	 * 成功
	 */
	SUCCESS,

	/**
	 * 失败
	 */
	FAIL,
}
