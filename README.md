## 废话篇

springbot+vue+ elementUI，用户权限管理系统，搭建，采用开源项目部署，可以商用。

2、试用于初学者，或者未参与工作开发的一些人群，项目相对简单，功能比较齐全。

3、源码下载地址：https://gitee.com/yoodb，从此地址下载springboot和vue前端框架，支持的功能有很多。简单看看吧。

- 用户管理：用户是系统操作人员，该功能主要完成对系统用户信息录入。
- 部门管理：维护系统组织机构（公司、部门、小组），树结构展现支持数据权限。
- 岗位管理：维护系统用户所属担任职务。
- 菜单管理：维护系统菜单，操作权限，按钮权限标识等信息。
- 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。还涉及数据权限管理。
- 字典管理：对系统中经常使用的一些较为固定的数据进行维护。
- 参数管理：对系统动态配置常用参数。
- 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
- 登录日志：系统登录日志记录查询包含登录异常。(^▽^)

4、将项目导入到idea中，因为之前上传的是eclipse开发工具，记着把eclispe的没用文件删掉。防止影响；

5、算了重新打开新的吧，之前我打开过，又删了。重新打开新文件吧，等待导入成功；

6、导入成功后执行maven install命令，下载jar包即可。执行完成。

7、项目中需要redis和mysql数据库，修改配置文件。application-druid.yml和application.yml

## 平台简介

该平台是基于若依框架的精简版，用户权限管理系统是一套用户权限管理且全部开源的快速开发平台，毫无保留给个人及企业免费使用，无版权纠纷问题。

* 前端采用Vue、Element UI搭建框架。
* 后端采用Spring Boot、Spring Security搭建框架，缓存采用Redis，数据库采用MySQL。
* 支持Jwt单点登录,权限认证使用Jwt，支持多平台认证系统。
* 支持动态加载权限菜单，多种方式权限控制。
* 高效率开发，使用代码生成器可以一键生成前后端代码。
* 特别鸣谢：[element](https://github.com/ElemeFE/element)，[eladmin-web](https://github.com/elunez/eladmin-web)，[vue-element-admin](https://github.com/PanJiaChen/vue-element-admin)，[RuoYi-Vue](https://gitee.com/y_project/RuoYi-Vue)。

## 内置功能

1.  用户管理：用户是系统操作人员，该功能主要完成对系统用户信息录入。
2.  部门管理：维护系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：维护系统用户所属担任职务。
4.  菜单管理：维护系统菜单，操作权限，按钮权限标识等信息。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
9.  登录日志：系统登录日志记录查询包含登录异常。(*^▽^*)

## 在线体验

- 账号/密码：admin/admin123 （注意：该账号无法登录）有什么技术问题可直接加我微信s101074110

演示地址：https://jingxuan.yoodb.com  【此链接已是微信小程序：Java精选面试题，后端管理系统，暂时不对外开放演示】

## 演示图

<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0415/104927_cdd9244c_853197.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0415/105618_56334e6a_853197.png"/></td>
    </tr>
    <tr><td><img src="https://images.gitee.com/uploads/images/2021/0415/110053_af7d2c87_853197.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2021/0415/110243_6a42dbfe_853197.png"/></td>
    </tr>
</table>


## 精选前后端分离交流群

问题解答，技术交流。专业微信技术交流群加微信s101074110，以下QQ群不在维护。

前端QQ群【精选开源 vue 交流组】：[![加入QQ群](https://img.shields.io/badge/650948240-blue.svg)](https://jq.qq.com/?_wv=1027&k=qJA4iUbz) 点击按钮入群。<br/>
后端QQ群【精选开源 java 交流组】：[![加入QQ群](https://img.shields.io/badge/248148860-blue.svg)](https://jq.qq.com/?_wv=1027&k=iG9WHyMO) 点击按钮入群。