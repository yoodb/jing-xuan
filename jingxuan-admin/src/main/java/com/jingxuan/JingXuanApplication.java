package com.jingxuan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 * 
 * @author 精选
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class JingXuanApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(JingXuanApplication.class, args);
        System.out.println("(*^▽^*) 精选管理系统启动成功！   (*^▽^*)  \n");
    }
}
