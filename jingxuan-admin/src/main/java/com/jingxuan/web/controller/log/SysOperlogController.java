package com.jingxuan.web.controller.log;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jingxuan.common.annotation.Log;
import com.jingxuan.common.core.controller.BaseController;
import com.jingxuan.common.core.domain.Result;
import com.jingxuan.common.core.page.TableDataInfo;
import com.jingxuan.common.enums.BusinessType;
import com.jingxuan.common.utils.poi.ExcelUtil;
import com.jingxuan.system.domain.SysOperLog;
import com.jingxuan.system.service.ISysOperLogService;

/**
 * 操作日志记录
 * 
 * @author 精选
 */
@RestController
@RequestMapping("/log/operlog")
public class SysOperlogController extends BaseController
{
    @Autowired
    private ISysOperLogService operLogService;

    @PreAuthorize("@ss.hasPermi('log:operlog:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysOperLog operLog)
    {
        startPage();
        List<SysOperLog> list = operLogService.selectOperLogList(operLog);
        return getDataTable(list);
    }

    @Log(title = "操作日志", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('log:operlog:export')")
    @GetMapping("/export")
    public Result export(SysOperLog operLog)
    {
        List<SysOperLog> list = operLogService.selectOperLogList(operLog);
        ExcelUtil<SysOperLog> util = new ExcelUtil<SysOperLog>(SysOperLog.class);
        return util.exportExcel(list, "操作日志");
    }

    @PreAuthorize("@ss.hasPermi('log:operlog:remove')")
    @DeleteMapping("/{operIds}")
    public Result remove(@PathVariable Long[] operIds)
    {
        return toresult(operLogService.deleteOperLogByIds(operIds));
    }

    @Log(title = "操作日志", businessType = BusinessType.CLEAN)
    @PreAuthorize("@ss.hasPermi('log:operlog:remove')")
    @DeleteMapping("/clean")
    public Result clean()
    {
        operLogService.cleanOperLog();
        return Result.success();
    }
}
